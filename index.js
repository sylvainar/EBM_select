var express = require('express');
app = express();
var server = require('http').Server(app);
var mysql = require('mysql');

// =====> CONFIGURATION DE LA BASE DE DONNEES
var db = mysql.createConnection({
  host     : 'localhost',
  user     : 'ebm',
  password : '',
  database : 'ebm'
});

// =====> ROUTES DE L'API
// Cas où on veut récupérer tous les étudiants
app.get("/api/students/",function(req,res){
  res.setHeader('Content-Type', 'application/json'); // JSON header
  db.query('SELECT * FROM etudiants', function(err, rows, fields) {
    // On renvoie toutes les lignes de la table étudiants sous
    // la forme d'un objet JSON
    res.send(JSON.stringify(rows));
  });
});

// Cas où on veut récupérer une liste d'étudiants qui commencent par :begin
app.get("/api/students/begin/:begin",function(req,res){
  res.setHeader('Content-Type', 'application/json');
  db.query('SELECT * FROM etudiants WHERE nom LIKE ?', [req.params.begin + '%'], function(err, rows, fields) {
    // On ne renvoit que les lignes dont le nom matchent le pattern '$begin%',
    // ce qui signifie, par exemple si $begin vaut A, on peut récupérer
    // tous les noms commençant par A. Cette requête n'est pas sensible à la
    // casse.

    //setTimeout(function() {
      res.send(JSON.stringify(rows));

    //}, 100);
    // Vous pouvez décommenter ces deux lignes pour ralentir l'API de façon
    // à voir l'icône de chargement.
  });
});

// =====> ROUTES DES PAGES WEB
// Chargement des fichiers statics (scripts, CSS, images...)
app.use('/dist', express.static('dist'));

// Afficher l'index
app.get('/', function(req,res){
  res.render('index.ejs');
});

// Afficher la page profil d'un élève
app.get('/student/:id', function(req,res){
  db.query('SELECT * FROM etudiants WHERE id=?', [req.params.id], function(err, rows, fields) {
    res.render('student.ejs',{student:rows[0]});
  });
});

// Lancement du serveur, qui va écouter sur le port 8080
server.listen(8080);
