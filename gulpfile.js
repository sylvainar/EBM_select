var gulp = require('gulp'); //Required
var sass = require('gulp-sass'); //Sass
var sequence = require('gulp-sequence'); //Make sequences
var uglify = require('gulp-uglify'); //Uglify JS
var bsConfig = require("gulp-bootstrap-configurator"); //Configure bootstrap
var concat = require("gulp-concat"); // Concat JS files

// Configure
var lib_list_js = ['./node_modules/jquery/dist/jquery.min.js'];
var lib_list_css = [];

// Vars
var bootstrap_config_file = "./config.json";
var src_scss = "./src/sass";
var src_js = "./src/js";
var dest_css = "./dist/css";
var dest_js  = "./dist/js";
var deploy_target = "";

// For Libs
gulp.task('libs', function() {
  gulp.src(lib_list_js)
    .pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(gulp.dest(dest_js))
  gulp.src(lib_list_css)
    .pipe(gulp.dest(dest_css))
});

// For CSS
gulp.task('make-bootstrap-css', function(){
  return gulp.src(bootstrap_config_file)
  .pipe(bsConfig.css({
    compress: true,
    bower: false
  }))
  .pipe(concat('libs.css'))
  .pipe(gulp.dest(dest_css));
});

// For JS
gulp.task('make-bootstrap-js', function(){
  return gulp.src(bootstrap_config_file)
  .pipe(bsConfig.js({
    compress: true,
    bower: false
  }))
  .pipe(concat('libs.js'))
  .pipe(gulp.dest(dest_js));
});

gulp.task('sass', function () {
  return gulp.src(src_scss + '/**/main.scss')
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(gulp.dest(dest_css));
});

gulp.task('js', function(){
  return gulp.src(src_js + '/**/*.js')
  .pipe(concat('scripts.js'))
  .pipe(uglify())
  .pipe(gulp.dest(dest_js));
});

gulp.task('watch', function () {
  gulp.watch('./src/**', ['sass', 'js']);
});

gulp.task('deploy', function(){
  return gulp.src('dist/**/*')
  .pipe(gulp.dest(deploy_target));
});

gulp.task('bootstrap', sequence('make-bootstrap-css','make-bootstrap-js'));
gulp.task('build', sequence('sass', 'js'));
gulp.task('build-libs', sequence('bootstrap','libs'));
gulp.task('default', sequence('build-libs','build', 'watch'));
