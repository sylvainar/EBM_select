function setSuggest(){

  $(document).ready(function(){
    // Uniquement quand le DOM est prêt, ce qui évite de
    // chercher après des objets qui n'existent pas encore.
    $(".input-suggest-container").each(function() {

      var suggestContainer = $(this);
      var url = suggestContainer.attr('url'); // On récupère l'URL à contacter
      var template_name = suggestContainer.attr('template'); // et le template à inflater

      // Pour chaque bloc de la classe input-suggest-container présent dans le document,
      // on va d'abord le peupler avec un input pour permettre la saisie du nom à rechercher, et un
      // bloc (pour l'instant vide) pour y mettre les résultats. La structure est la suivante :
      // --- input-suggest-container
      // ------ input-suggest-group (permet de mettre les deux blocs ci dessous à la même largeur)
      // --------- input
      // --------- ul class="suggest-list"
      // ------ button-reset
      var htmlToAppend =
      '<div class="input-suggest-group">'
      + '<input type="text" placeholder="' + suggestContainer.attr('placeholder') + '" class="form-control">'
      + '<ul class="suggest-list"></ul>'
      + '</div>'
      + '<div class="button-reset"><img src="dist/img/delete.png" /></div>'
      + '</div>';
      suggestContainer.append(htmlToAppend);

      // On gère le click sur la petite croix
      suggestContainer.find('.button-reset').click(function(){
        suggestContainer.find('input').val(''); // On vide le champ
        suggestContainer.find('.suggest-list').html(''); // On vide les suggestions
      });

      // On va lier au champ la fonction goSuggest qui va s'exécuter à chaque
      // évenement de type KeyUp
      suggestContainer.keyup(function(){
        goSuggest(suggestContainer, url, template_name); // Et on exécute la fonction
      });
    });
  });

  // Cette fonction effectue l'appel Ajax puis inflate le résultat dans le DOM.
  // Elle prend en paramètre l'élement à surveiller, l'url à contacter et le
  // template à inflater à la fin.
  function goSuggest(suggestContainer, url, template_name){

    var imgReset = suggestContainer.find('.button-reset img'); // On récupère l'image
    var begin = suggestContainer.find('input').val(); // On récupère la valeur du champ de formulaire
    var stringToInflate = "";

    if(begin == '') // Si le champ est vide, on ne fait pas d'appel, et on vide le container en dessous
    {
      suggestContainer.find('.suggest-list').html('');
    }
    else
    {
      imgReset.attr('src','dist/img/ajax-loader.gif'); // Avant de lancer l'appel, on change le bouton

      $.ajax({
        url:  url + begin // On concatène pour obtenir l'adresse de l'API à contacter
      })
      .done(function( data ) {
        imgReset.attr('src','dist/img/delete.png'); // On a eu une réponse, on remet la croix

        // Pour chaque objet dans le JSON de résultat, on concatène à la
        // chaîne de retour le template qui a été défini.
        // La syntaxe window[template_name] permet tout simplement d'utiliser la
        // fonction portant le nom "template_name"
        data.forEach(function(row){
          stringToInflate += '<li>'+window[template_name](row, begin)+'</li>';
        })

        // Et maintenant, on inflate la chaîne dans le DOM
        suggestContainer.find('.suggest-list').html(stringToInflate);
      });
    }
  }

  console.log("Suggest Lib loaded");
}
